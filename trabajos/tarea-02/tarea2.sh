#!/bin/bash

#requisitos del scrip
#Guargar este scrip en /home/ y agregar la siguiente linea al final del  archivo /etc/bash.bashrc:
#bash /home/tarea-02.bs
#crear la carpeta /logs/ si no existe

#variables
export FECHA="$(date "+%d-%m-%Y-%H:%M:%S")"
BACKUP="backup$USER-$FECHA"
UBICACION="/home/backup"

#1 crear backup la carpeta del usuario en la carpeta /home/backup
#inicia proceso de backup
echo "$FECHA-Inicia el proceso de backup" >> $UBICACION/backups.log

#realiza la copia de la carpeta del usuario y registra el log en backup.log
cp -r -a $HOME $UBICACION/$BACKUP 2>>$UBICACION/backup.log \
	&& echo "$FECHA-Finalizo correctamente el proceso de backup" >> $UBICACION/backup.log \
	|| echo "$FECHA-Finalizo con error el proceso de bakcup" >> $UBICACION/backup.log

#2 log inicio de sesion
echo "$FECHA - $USER Inicio sesion" >> /logs/userlogin.log
echo "Registro de ingreso: $FECHA" >> /logs/login_$USER.log

#3 

export ESPACIO=$(du -c $HOME | grep total)
export SO=$(uname -v)

bash /home/tarea-02_3.sh

