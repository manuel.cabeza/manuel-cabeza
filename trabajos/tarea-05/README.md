

Trabajo Practico numero 5 - Programacion

   Alumno: Manuel Cabeza

Para utilizar este sript puede hacerlo con argumentos o sin argumentos para ir directamente al menu.

Debe ser ejecutado con permisos de root.

Sintaxis:
     tarea.sh [OPCION] [ARGUMENTO] [ARGUMENTO] ...


OPCIONES:
    --help          
        Muestra este archivo para aprender a utilizar el script.

    [menu] 
        Utiliza el menu principal para facilitar el uso del script.

    [-i | ip]      [<direccion ip>] [<mascara de red>] [<puerta de enlace>]
        Cambiar la direccion ip, mascara de red y puerta de enlace del servidor.

    [-h | hostname] [<nombre del equipo>]
        Cambiar el hostname del equipo.
    
    [-inst | install] [<componente>]
       Instalar componentes en el sistema.

    [-unin | uninstall] [<componente>]
        Desinstalar componentes en el sistema.

    [-a | add | agregar] [<dominio del sitio>]
        Agregar nuevos sitios al sistema.

    [-d | del | borrar] [<dominio del sitio>]
        Aorrar sitios existentes del sistema.

    [-m | mod | modificar] [<dominio del sitio>] [en | dis]
        Habilitar[en] o deshabilitar[dis] un sitio del sistema.

    [-l | log] [<dominio del sitio>] [<Destino>]
        Cambiar hubicacion de destino de log de los sitios del sistema.

    [-w | wp | wordpress] [<dominio del sitio>] [<usuario DB>]
        Instala y realiza la configuracion basica de wordpress.

    [-b | bk | backup] [<dominio del sitio>]
        Realiza el backup del sitio indicado. La ubicacion del destino se define unicamente la primera vez que se utiliza la opcion.

    [migrar] [<dominio del sitio>]
        Mueve todos los archivos de un sitio a otro servidor.



