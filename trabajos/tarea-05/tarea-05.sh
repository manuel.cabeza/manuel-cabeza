#!/bin/bash
#
# Trabajo Practico numero 5 - Programacion
#
#   Alumno: Manuel Cabeza
#
# Para utilizar este sript puede hacerlo con argumentos o sin argumentos para ir directamente al menu.
# Ejemplo: tarea-05.sh menu → ejecuta inicia con el menu
#          tarea-05.sh ip <direccion ip> <mascara de red> <puerta de enlace> → cambia la direccion ip del sistema
#           --help  → Mas informacion para el uso del script


source ./tp05.conf
source ./menu
source ./funciones_tp05


verif_root

[ -z "$TP05LOGS" ] && config_dir "logs de $0" TP05LOGS
[ -z "$1" ] && menu_principal

case $1 in
    --help) ayuda ;;
    ip | -i) cambiar_ip $2 $3 $4;;
    install | -inst) install_componente $2 ;;
    uninstall | -unin) uninstall_componente $2 ;;
    hostname | -h) cambiar_hostname $2 ;;
    add | agregar | -a) add_sitio $2 ;;
    del | borrar | -d) del_sitio $2 ;;
    mod | modificar | -m) mod_sitio $2 $3 ;;
    log | -l) mod_log $2 $3;;
    wp | wordpress | -w) mod_wordpress $2 $3 ;;
    bk | backup | -b) backup $2 ;;
    migrar) migrar_sitio $2 $3 $4;;
    menu) menu_principal ;;
    *) echo "Argumento invalido" && exit 1 ;;
esac

exit 0



