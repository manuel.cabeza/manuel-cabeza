#!/bin/bash

# Verificar si se ejecuta como root
if [ $UID != 0 ];then
    echo "Ejecute el sript como root."
    exit 1
fi

# Verifica si esta instalado ufw
if ! apt search ufw | grep "\[instal" 1>/dev/null;then
    read -p "No cuenta el el paquete UFW. Se procedera a instalarlo. ¿Desea continuar?  (S)i, (N)o " RESP
    case $RESP in
        S | s)
            sudo apt-get install ufw
        ;;
        *)
            echo "No se instalo el paquete necesario para utilizar este script."
            exit 0
        ;#!/bin/bash

# Verificar si se ejecuta como root
if [ $UID != 0 ];then
    echo "Ejecute el sript como root."
    exit 1
fi

# Verifica si esta instalado ufw
# if ! apt search ufw | grep "\[instal" 1>/dev/null;then
#     read -p "No cuenta el el paquete UFW. Se procedera a instalarlo. ¿Desea continuar?  (S)i, (N)o " RESP
#     case $RESP in
#         S | s)
#             sudo apt-get install ufw
#         ;;
#         *)
#             echo "No se instalo el paquete necesario para utilizar este script."
#             exit 0
#         ;;
#     esac
# fi

while :;do
    #menu principal
    echo "Menu:
        1) Cambiar politicas por defecto.
        2) ABM de reglas.
        3) Cambiar la posicion de las reglas.
        4) Mostrar las reglas.
        5) Backup de reglas.
        6) Carga del firewall con el sistema.
        7) Salir"
    read -p "Ingrese el numero de la opcion: " RESP
    case $RESP in
        1)
            #cambio de politicas por defecto
            echo "1) Cambiar politicas por defecto."
            #el usuario elige que tipo de politica quiere modificar.
            read -p "¿Que tipo de destino de paquetes quiere modificar? (E)ntrantes, (S)alientes" RESP
            case $RESP in
                E | e)
                    # el usuario define si quiere permitir o denegar las politicas incoming
                    read -p "¿Que tipo de politicas quiere aplicar por defecto para los paquetes entrantes? (P)ermitir, (D)enegar. " RESP
                    case $RESP in
                        P | p | Permitir)
                            #verifica si se acepta realizar el cambio de la politica
                            read -p "¿Esta seguro que quiere permitir por defecto todos los paquetes entrantes? (S)i, (N)o" RESP
                            case $RESP in
                                S)
                                    #realiza el cambio a la regla por defecto.
                                    #ufw default allow incoming
                                    iptables -P INPUT ACCEPT
                                    echo "Se permitiran por defecto todos los paquetes entrantes."
                                ;;
                                *) 
                                    # se cancela el cambio y vuelve vuelve al menu principal.
                                    echo "No se realizo ningun cambio."
                                    continue
                                ;;
                            esac
                        ;;
                        D | d | Denegar)
                            #verifica si se acepta realizar el cambio de la politica
                            read -p "¿Esta seguro que quiere denegar por defecto todos los paquetes entrantes? (S)i, (N)o" RESP
                            case $RESP in
                                S)
                                    #realiza el cambio a la regla por defecto.
                                    #ufw default deny incoming
                                    iptables -P INPUT DROP
                                    echo "Se denegaran por defecto todos los paquetes entrantes."
                                ;;
                                *) 
                                    # se cancela el cambio y vuelve vuelve al menu principal.
                                    echo "No se realizo ningun cambio."
                                    continue
                                ;;
                            esac
                        ;;
                        *)
                            # se cancela el cambio y vuelve vuelve al menu principal.
                            echo "Ingreso una opcion incorrecta. no se realizara ningun cambio."
                           continue 
                        ;;
                    esac
                ;;
                S | s)
                    #El usuario define la politica para Outgoing
                    read -p "¿Que tipo de politicas quiere aplicar por defecto para los paquetes salientes? (P)ermitir, (D)enegar. " RESP
                    case $RESP in
                        P | p | Permitir)
                            #verifica si se acepta realizar el cambio de la politica
                            read -p "¿Esta seguro que quiere permitir por defecto todos los paquetes salientes? (S)i, (N)o" RESP
                            case $RESP in
                                S | s | Si | si)
                                    #realiza el cambio a la regla por defecto.
                                    #ufw default allow outgoing
                                    iptables -P OUTPUT ACCEPT
                                    echo "Se permitiran por defecto todos los paquetes salientes."
                                ;;
                                *) 
                                    # se cancela el cambio y vuelve vuelve al menu principal.
                                    echo "No se realizo ningun cambio."
                                    continue
                                ;;
                            esac
                        ;;
                        D | d | Denegar)
                            #verifica si se acepta realizar el cambio de la politica
                            read -p "¿Esta seguro que quiere denegar por defecto todos los paquetes salientes? (S)i, (N)o" RESP
                            case $RESP in
                                S)
                                    #realiza el cambio a la regla por defecto.
                                    #ufw default deny outgoing
                                    iptables -P OUTPUT DROP
                                    echo "Se denegaran por defecto todos los paquetes salientes."
                                ;;
                                *) 
                                    # se cancela el cambio y vuelve vuelve al menu principal.
                                    echo "No se realizó ningun cambio."
                                    continue
                                ;;
                            esac
                        ;;
                        *)
                            # se cancela el cambio y vuelve vuelve al menu principal.
                            echo "Ingreso una opcion incorrecta. no se realizara ningun cambio."
                            continue
                        ;;
                    esac
                ;;
                *)
                    # se cancela el cambio y vuelve vuelve al menu principal.
                    echo "Ingreso una opcion incorrecta. no se realizara ningun cambio."
                    continue
                ;;
            esac
        ;;
        2)
            echo "2) ABM de reglas."
            #agregar o borrar reglas
            read -p "¿Quiere (A)gregar o (B)orrar una regla?" RESP
            case $RESP in
                A | a | Agregar)
                    #regla para permitir o denegar
                    echo "Defina la desicion de la regla.
                        (P)ermitir
                        (D)enegar"
                    read -p "Elija una opcion: " RESP
                    case $RESP in
                        P | p | Permitir)
                            #la regla es para permitir(allow)
                            #PERMISO=allow
                            PERMISO=ACCEPT
                        ;;
                        D | d | Denegar)
                            #la regla es para denegar(deny)
                            #PERMISO=deny
                            PERMISO=DROP
                        ;;
                        *)
                            # se cancela el cambio y vuelve vuelve al menu principal.
                            echo "Opcion incorrecta. No se realizó ningun cambio."
                            continue
                        ;;
                    esac
                    echo "Defina el puerto/protocolo:"
                    #ejemplos: ssh; 22/tpc; 80/tcp; www;
                    #Tambien puede ingresar un rango de puertos utilizando ':'
                    #ejemplo:  1000:2000/tpc; 1000:2000/udp"
                    read -p "Ingrese el numero puerto: " PUERTO
                    read -P "Se aplicara el permiso $PERMISO al puerto $PUERTO. ¿Quiere continuar? (S)i,(N)o " RESP
                    case $RESP in
                        S | s | Si | si)
                            #ufw $PERMISO $PUERTO 2> log.txt
                            iptables -A INPUT -p tcp --dport $PUERTO -j $PERMISO 
                            #en caso de haber algun error guarda el mensaje de error en el fichero log.txt y sale con exit status 1
                            if [ $? == 1 ]; then
                                echo "ERROR - ver log.txt"
                                exit 1
                            fi
                            echo "Se permitiran por defecto todos los paquetes salientes."
                        ;;
                        *) 
                            echo "No se realizo ningun cambio."
                        ;;
                    esac
                ;;
                B | b | Borrar)
                    echo "Defina la desicion de la regla a borrar.
                        (P)ermitir
                        (D)enegar"
                    read -p "Elija una opcion: " RESP
                    case $RESP in
                        P | p | Permitir)
                            #la regla es para permitir(allow)
                            #PERMISO=allow
                            PERMISO=ACCEPT
                        ;;
                        D | d | Denegar)
                            #la regla es para denegar(deny)
                            #PERMISO=deny
                            PERMISO=DROP
                        ;;
                        *)
                            # se cancela el cambio y vuelve vuelve al menu principal.
                            echo "Opcion incorrecta. No se realizó ningun cambio."
                            continue
                        ;;
                    esac
                    echo "Defina el puerto/protocolo:
                    ejemplos: ssh; 22/tpc; 80/tcp; www;
                    Tambien puede ingresar un rango de puertos utilizando ':'
                    ejemplo:  1000:2000/tpc"
                    read -p "Ingrese el puerto: " PUERTO
                    read -P "Se borrara la regla $PERMISO al puerto $PUERTO. ¿Quiere continuar? (S)i,(N)o " RESP
                    case $RESP in
                        S | s | Si | si)
                            #ufw delete $PERMISO $PUERTO 2> log.txt
                            iptables -D INPUT -p tcp --dport $PUERTO -j $PERMISO
                            #en caso de haber algun error guarda el mensaje de error en el fichero log.txt y sale con exit status 1
                            if [ $? == 1 ];then
                                echo "ERROR - ver log.txt"
                                exit 1
                            fi
                            echo "Se elimino la regla."
                        ;;
                        *) 
                            echo "No se realizo ningun cambio."
                            continue
                        ;;
                    esac
                ;;
                *)
                    echo "Opcion incorrecta. No se realizó ningun cambio."
                    continue
                ;;
            esac
        ;;
        3)

        ;;
        4)
            #lista las reglas con los numeros para identificarlos mejor.
            #sudo ufw status numbered
            iptables -nvL
            read -p "Presione (Enter) para volver al menu... "
        ;;
        5)
            echo "5) Backup de reglas."
            read -vp "Ingrese la ruta completa para guardar el backup: " $RUTA
            REGLAS=/etc/ufw/user.rules
            if [ -d $RUTA ]; then
                cp $REGLAS $RUTA 2> log.txt
                #en caso de haber algun error guarda el mensaje de error en el fichero log.txt y sale con exit status 1
                if [ $? == 1 ];then
                    echo "ERROR - ver log.txt"
                    exit 1
                fi
            fi
        ;;
        6)
            
        ;;
        7)
            exit 0
        ;;
        *) 
            echo "Ingrese una opcion correcta."
        ;;
    esac
done;
