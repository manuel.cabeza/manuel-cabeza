#funciones para la tarea-05 de programacion

#verifica si el script fue ejecutado con permisos de root
verif_root () {
    if [ $UID -ne 0];then
        echo "Necesita ejecutar el script como root" && exit 1
    fi
}

#en caso de no confirmar o algun error sale del script o vuelve al menu principal
cancel () {
    echo "No se realizo ningun cambio."
    if [[ $MENU == 1 ]]; then
        read -p "Presione ENTER para continuar."
        menu_principal
    else
        exit 1
    fi
}

#solicita la confirmacion del usuario al momento de realizar cualquier cambio.
confirmacion () {
    while :;do
        read -p "¿Desea continuar? [Si=s;No=n]" RESP
        case $RESP in
            s | S) return 0 ;;
            n | N) cancel ;;
            *) echo "ingrese una opcion valida." ;;
        esac
    done
}

#En caso de que haya algun error en los comandos termina la ejecucion de script
fun_error () {
    echo "Hubo un error durante el proceso. Revise el archivo $TP05LOGS para mas detalle."
    exit 1
}

#modifica un archivo ya existente tomando el mismo como referencia
modif_archivo () {
    ARCHIVO=$1
    DATO=$2
    CAMBIO=$3
    cp $ARCHIVO bk.tmp
    cat bk.tmp | sed "s!${DATO}!${CAMBIO}!g" > ${ARCHIVO} 2>> $TP05LOGS || fun_error
}

#configura la rutas para los backup o logs
config_dir () {
    echo "No esta configurada la ruta para los $1. Por favor defina una."
    read -ep "Ingrese el directorio: " DIR
    [ -z "$TP05LOGS" ] && TP05LOGS=${DIR}logs.txt && DIR="${DIR}log.txt"
    modif_archivo final.conf "$2=" "$2=$DIR"
    echo "Se configuró la ruta $DIR para los $1."
}

#verifica si la ip recibida es válida. con formato 1.1.1.1 con valores no mayores a 255.
validar_ip () {
    local IP=$1
    if [[ $IP =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        IP=($IP)
        IFS=$OIFS
        for i in ${IP[@]}; do
            [[ $i -le 255 ]] || return 1
        done
        return 0
    fi
    return 1
}

interfaces(){
    #busca las intefaces exitentes y las guarda en un vector
    INTER=($(ip -o link | cut -d ":" -f 2))
    #calcula el tamaño del vector para recorrerlo
    FIN=$((${#INTER[@]} - 1))
    echo "Interfaces disponibles:"
    #menu dinamico con los nombres de las interfaces
    for ((i=0;i<=$FIN;i++)); do
        echo "  $(( $i + 1 ))) ${INTER[$i]}"
    done
    #eleccion de la interface
    read -p "elija la interfaz de red: " RESP
    if [[ $RESP -le $i ]]; then
        INTER=${INTER[$(($RESP - 1))]}
    else
        echo "Opcion Incorrecta."
        cancel
    fi
}

validar_inter () {
    local INTER=($(ip -o link | cut -d ":" -f 2))
    for i in ${INTER[@]}; do
        [[ $1 = $i ]] && return 0
    done
    return 1
}

#Cambiar direccion ip, mascara de red y puerta de enlace
cambiar_ip () {
    INTER=$1
    IP=$2
    MASK=$3
    GTWAY=$4
    [ -z "$INTER" ] && interfaces
    validar_inter $INTER || (echo "Interfaz inexistente." && cancel)
    [ -z "$IP" ] && read -p "Ingrese la ip para el servidor: " IP
    validar_ip $IP || (echo "La ip ingresada es invalida." && cancel)
    [ -z "$MASK" ] && read -p "Ingrese la mascara de red: " MASK
    validar_ip $MASK || (echo "La mascara de red ingresada es invalida." && cancel)
    [ -z "$GTWAY" ] && read -p "Ingrese la puerta de enlace: " GTWAY
    validar_ip $GTWAY || (echo "La puerta de enlace ingresada es invalida." && cancel)

    echo "Se realizará el siguiente cambio a la interface $INTER: 
    IP: $IP
    Mascara de red: $MASK
    Puerta de enlace: $GTWAY"
    echo -e "ADVERTENCIA: si esta conectado por remoto puede perder la conexion con el servidor.\nSe generará una copia de la configuracion antes de realizar el cambio."
    if confirmacion ; then
        RED=(address netmask gateway)
        NETWK=($IP $MASK $GTWAY)
        ARCHIVO=/etc/network/interfaces
        cp $ARCHIVO bknet
        #calcula la linea del archivo donde esta la interfaz elegida
        LINEA=$(cat ${ARCHIVO} | grep -n "iface ${INTER}" | cut -d ":" -f 1)
        #cambia la configuracio de dhcp a static
        sed -i "/${INTER}/s/dhcp/static/"  ${ARCHIVO}
        #verifica si 
        if cat ${ARCHIVO} | grep -A 1 ${INTER} | grep ${RED} ;then
                for i in {0..2}; do
                        DATO=$(cat bknet | grep -A 4 ${INTER} | grep ${RED[$i]})
                        sed -i "s/${DATO}/      ${RED[$i]} ${NETWK[$i]}/" ${ARCHIVO} 2>> log.txt
                done
        else
                for i in {0..2}; do
                        sed -i "$((${LINEA} + $i)) a ${RED[$i]} ${NETWK[$i]}" ${ARCHIVO}
                done
        fi
    fi

}

#cambiar nombre del equipo
cambiar_hostname () {
    echo "Cambiara el nombre del sistema por $1" 
    confirmacion && hostnamectl set-hostname $1 2>> $TP05LOGS || fun_error
    echo "Se reiniciara el sistema."
    confirmacion && reboot || echo "Debera reiniciar el sistema para aplicar el cambio."
}

#instala componentes especificos o todos.
install_componente () {
    [ -z "$1" ] && echo "No ingreso ningun componente para instalar." && cancel
    if [ $1 != "lamp" ];then
        if apt search $1 | grep "\[instal" 1>/dev/null ;then
            echo "El componente $1 ya esta instalado en el sistema."
        else
            echo "No cuenta con el paquete $1. Se procedera a instalarlo. " 
            confirmacion && apt update && apt install $1 2>> $TP05LOGS || cancel
            if [[ $1 == "apache2" ]]; then
                echo -e "#Include generic snipperts of statemen \nIncludeOptional conf-enabled/*.conf" >> /etc/apache2/apache.conf || fun_error
            fi
            systemctl restart $1 2>> $TP05LOGS || fun_error
            echo "Instalacion completa."
        fi
    else
        lamp=(apache2 default-mysql-server php libapache2-mod-php php-mysql)
        for i in ${lamp[@]};do
            if ! apt search ${lamp[$i]} | grep "\[instal" 1>/dev/null;then
                echo "No cuenta con el paquete ${lamp[$i]}. Se procedera a instalar los componentes de LAMP. " 
                if confirmacion ; then
                    apt update 2>> $TP05LOGS || fun_error
                    apt install apache2 default-mysql-server php libapache2-mod-php php-mysql 2>> $TP05LOGS || fun_error
                    echo -e "\nInstalacion completa con exito."
                    break
                fi
            fi
        done
    fi
}

firewall () {
    echo "Proximamente."
}

#desinstala los componentes
uninstall_componente () {
    [ -z "$1" ] && echo "No ingreso ningun componente para desinstalar." && cancel
    COMP=$1
    echo "Se desinstalara el comonente $COMP"
    if confirmacion ;then 
        service $1 stop 2>> $TP05LOGS || fun_error
        apt-get purge $1* 2>> $TP05LOGS || fun_error
        apt-get autoremove 2>> $TP05LOGS || fun_error
        echo -e "\nDesinstalacion completa con exito."
    fi
}

#agregar un dominio a los sitios del apache
add_sitio () {
    [ -z "$1" ] && echo "No ingreso ningun sitio para agregar." && cancel
    DOMINIO=$1
    [ -d /var/www/$DOMINIO ] && echo "El sitio ya existe. si continua es posible que se borre informacion." || echo "Se creara el sitio $DOMINIO."
    if confirmacion ;then
        mkdir -p /var/www/$DOMINIO 2>> $TP05LOGS || fun_error
        echo "<h1>Hola Mundo! Bienvenido al sitio $DOMINIO</h1>" > /var/www/$DOMINIO/index.html
        VHOST=(ServerAdmin ServerName ServerAlias DocumentRoot)
        VALOR=(admin@$DOMINIO $DOMINIO www.$DOMINIO /var/www/$DOMINIO)
        cp vhost_template.txt /etc/apache2/sites-available/$DOMINIO.conf
        for i in {0..3}; do
            sed -i "s!${VHOST[$i]}!${VHOST[$i]} ${VALOR[$i]}!g" /etc/apache2/sites-available/$DOMINIO.conf 2>> $TP05LOGS || fun_error
        done
        a2ensite $DOMINIO.conf 2>> $TP05LOGS || fun_error
        a2dissite 000-default.conf 2>> $TP05LOGS || fun_error
        systemctl reload apache2 2>> $TP05LOGS || fun_error
        echo "Sitio creado. Recuerde modificar los DNS para poder acceder correctamente al sitio."
    fi
    
}

ver_sitios () {
    echo "Los sitios disponibles son: "
    ls /var/www/
}

#elimina un sitio
del_sitio () {
    [ -z "$1" ] && echo "No ingreso ningun sitio para borrar." && cancel
    DOMINIO=$1
    [ -d /var/www/$DOMINIO ] || (echo "El sitio $DOMINIO no existe." && cancel)
    echo "Se eliminara el sitio $DOMINIO."
    if confirmacion ;then
        a2dissite $DOMINIO.conf 2>> $TP05LOGS || fun_error
        rm -Rf /var/www/$DOMINIO 2>> $TP05LOGS || fun_error
        rm -f /etc/apache2/sites-available/$DOMINIO.conf 2>> $TP05LOGS || fun_error
        systemctl reload apache2 2>> $TP05LOGS || fun_error
        echo "Sitio eliminado."
    fi
}

#habilita o deshabilita un sitio
mod_sitio () {
    [ -z "$1" ] && echo "No ingreso ningun sitio para modificar." && cancel
    [ -z "$2" ] && echo "No indico si quiere habiliar o deshabiliar el sitio $1." && cancel
    DOMINIO=$1
    [ -d /var/www/$DOMINIO ] || (echo "El sitio $DOMINIO no existe." && cancel)
    case $2 in
        en) echo "Habiliara el sitio $DOMINIO"
            confirmacion && a2${2}site $DOMINIO.conf && systemctl reload apache2 2>> $TP05LOGS || fun_error ;;
        dis) echo "Deshabiliara el sitio $DOMINIO"
            confirmacion && a2${2}site $DOMINIO.conf && systemctl reload apache2 2>> $TP05LOGS || fun_error ;;
        *) echo "Argumento incorrecto." | tee -a $TP05LOGS && cancel ;;
    esac
}

#modifica la ubicacion de los logs
mod_log () {
    [ -z "$1" ] && echo "No ingreso el nombre del sitio." && cancel
    [ -z "$2" ] && echo "No ingreso el destino para los logs." && cancel
    DOMINIO=$1
    DIRLOGS=$2
    [ -d /var/www/$DOMINIO ] || (echo "El sitio $DOMINIO no existe." && cancel)
    [ -d $DIRLOGS ] || mkdir $DIRLOGS
    CONFDOM=/etc/apache2/sites-available/$DOMINIO.conf
    CUSLOG=$(cat ${CONFDOM} | grep "CustomLog")
    ERRLOG=$(cat ${CONFDOM} | grep "ErrorLog")
    echo "Se modificara la ruta de los logs del sitio $DOMINIO por $DIRLOGS"
    if confirmacion ;then
        modif_archivo $CONFDOM "$CUSLOG" "CustomLog ${DIRLOGS}access.log combined"
        modif_archivo $CONFDOM "$ERRLOG" "ErrorLog ${DIRLOGS}error.log combined"
        systemctl reload apache2 2>> $TP05LOGS || fun_error
    else
        cancel
    fi
}

#instala todos los componente necesarios para wordpress, ademas de wordpress, y realiza las configuraciones basicas.
mod_wordpress () {
    WORDPRESS=1
    [ -z "$1" ] && echo "No ingreso el nombre del sitio." && cancel
    [ -z "$2" ] && echo "No ingreso el usuario para la base de datos." && cancel
    DOMINIO=$1
    USU=$2
    [ -d /var/www/$DOMINIO ] || (echo "El sitio $DOMINIO no existe." && cancel)
    read -sp "Ingrese la contraseña para la base de datos de wordpress: " PASS
    install_componente all
    mysql -u root -p -e "CREATE DATABASE wordpress;"
    mysql -u root -p -e "CREATE USER ${USU}@localhost IDENTIFIED BY '${PASS}';"
    mysql -u root -p -e "GARNT ALL ON wordpress.* TO 'user'@'localhost';"
    mysql -u root -p -e "FLUSH PRIVILEGES;"
    wget https://wordpress.org/latest.tar.gz 2>> $TP05LOGS || fun_error
    tar -xf latest.tar.gz /var/www/$DOMINIO/html 2>> $TP05LOGS || fun_error
    RUTA="/var/www/$DOMINIO/html/wordpress"
    cp ${RUTA}/wp-config-sample.php ${RUTA}/wp-config.php 2>> $TP05LOGS || fun_error
    DEFAULT=(username_here password_here)
    DATOS=($USU $PASS)
    for i in {0..1}; do
        modif_archivo ${RUTA}/wp-config.php ${DEFAULT[$i]} ${DATOS[$i]}
    done
}

#realiza los bakups de los sitios
backup () {
    [ -z "$1" ] && echo "No ingreso el nombre del sitio." && cancel
    [ -z "$DIRBACKUP" ] && config_dir "backups" "DIRBACKUP" && DIRBACKUP=$DIR
    [ -d ${DIRBACKUP} ] || mkdir ${DIRBACKUP}
    DOMINIO=$1
    [ -d /var/www/$DOMINIO ] || (echo "El sitio $DOMINIO no existe." && cancel)
    DATE=$(date +%d-%m-%Y-%H:%M:%S)
    echo "Se hara el backup del sitio $DOMINIO en $DIRBACKUP"
    confirmacion && tar -czvf ${DIRBACKUP}bkp$DOMINIO-$DATE.tar.gz /var/www/$DOMINIO 2> $TP05LOGS || fun_error
    echo "Backup del sitio $DOMINIO ha finalizado con exito."
}

#Migra un sitio a otro servidor
migrar_sitio () {
    echo "En construccion." #&& cancel
    [ -z "$1" ] && echo "No ingreso el nombre del sitio." && cancel
    DOMINIO=$1
    [ -d /var/www/$DOMINIO ] || (echo "El sitio $DOMINIO no existe." && cancel)
    USU=$2
    DESTINO=$3
    scp /etc/apache2/sites-available/$DOMINIO.conf $USU@$DESTINO:/etc/apache2/sites-available/ 2>> $TP05LOGS || fun_error
    scp -r /var/www/$DOMINIO $USU@$DESTINO:/var/www 2>> $TP05LOGS || fun_error
    ssh $USU@$DESTINO 'a2ensite $DOMINIO.conf' 2>> $TP05LOGS || fun_error
    ssh $USU@$DESTINO 'systemctl reload apache2' 2>> $TP05LOGS || fun_error
}

#verifica si el script fue ejecutado con permisos de root
verif_root () {
    if [[ $UID != 0 ]];then
        echo "Necesita ejecutar el script como root" && exit 1
    fi
}

#llama al archivo README.md que es donde se desbrive el uso del script
ayuda () {
    cat README.md 2>> $TP05LOGS || fun_error
}


