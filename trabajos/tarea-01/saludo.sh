#!/bin/bash
clear

echo "Bienvenido $USER! Es un gusto tenerte otro vez por aca!"
echo ""

echo "Hoy es ..."
date
echo ""

echo "Asegurate de guardar tus archivos en tu carpeta personal. Esta es $HOME"
echo ""

echo "Estos son tus archivos hasta ahora:"
ls -R $HOME
echo ""
