#!/bin/bash

#Verificar si se ejecuto el script como root
if [ ! $UID -eq 0 ]; then
    echo "Se recomienda ejecutar el script $0 como usuario root para utilizar todas las funciones."
    read -p "¿Desea Salir?[s=Si/n=No]: " RESP
    #verifica la respuesta
    i=0
    while [ $i != "1" ]; do
        if [[ ${RESP,,} == "s" ]]; then
            exit 0
        elif [[ ${RESP,,} == "n" ]]; then
            echo "Recuerde que no podrá utilizar todas las opciones."
            i=1
        else
            read -p "Ingrese 's' para Salir o 'n' para continuar: " RESP
        fi
    done
fi

#verificar e instalar los comandos necesarios

#comienzo del menu
while : ; do
    clear
    echo "Menu principal:
    1) Cambiar permisos de archivos o directorios.
    2) Cambiar el dueño o grupo de ficheros o directorios.
    3) Crear o eliminar un grupo de usuarios.
    4) Listar grupos existentes.
    5) Buscar fichero o directorio con permisos determinados.
    6) Salir."
    read -p "Ingrese el numero para elegir la opcion: " RESP
    
    if [[ $RESP == "1" ]]; then
        #opcion 2: Cambiar permiso de archivo o directorio elegido por el usuario.
        echo -e "\nCambiar permisos de archivos o directorios."

        #si no es root el que ejecuta muestra un mensaje recordando que tendra limitaciones.
        if [ ! $UID -eq 0 ]; then
            echo "Recuerde que si no ejecuta el srcipt como root no va a poder cambiar algunos permisos."
        fi

        read -ep "Ingrese el nombre del archivo: " ARCHIVO

        #verifica si el archivo existe
        if [ -a "$ARCHIVO" ]; then
            #si el archivo existe, pregunta que tipo de permisos se van a cambiar.
            i=0
            while [ $i != "1" ]; do
                read -p "¿Que permiso desea cambiar?[U=Usuario,G=Grupo,O=Otros,A=Todos] " PERMISO
                if [[ ${PERMISO,,} == "u" || ${PERMISO,,} == "g" || ${PERMISO,,} == "o" || ${PERMISO,,} == "a" ]]; then
                    #si el valor ingresado es correcto, sigue avanzando.
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    echo "Ingrese una opcion correcta."
                fi
            done

            i=0
            while [ $i != "1" ]; do
                read -p "¿Quiere añadir o quitar el permiso?['+'=Añadir,'-'=Quitar] " CAMBIO
                if [[ $CAMBIO == "+" || $CAMBIO == "-" ]]; then
                    #si el valor ingresado es correcto, sigue avanzando.
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    echo "Ingrese una opcion correcta."
                fi
            done

            i=0
            while [ $i != "1" ]; do
                read -p "¿Que tipo de permiso quiere modificar?[r=Lectura,w=Escritura,x=Ejecución] " TIPO
                if [[ ${TIPO,,} == "r" || ${TIPO,,} == "w" || ${TIPO,,} == "x" ]]; then
                    #si el valor ingresado es correcto, sigue avanzando.
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    echo "Ingrese una opcion correcta."
                fi
            done

            #realiza el cambio de permiso elegido
            if [[ ${PERMISO,,} == "a" ]]; then
                echo "Se asignará el permiso $CAMBIO${TIPO,,} para todos al archivo o directorio $ARCHIVO.
            [U=Usuario,G=Grupo,O=Otros,A=Todos]
            ['+'=Añadir,'-'=Quitar]
            [r=Lectura,w=Escritura,x=Ejecución]"
                read -p "¿Desea continuar?[s=Si/n=No] " RESP
                i=0
                while [ $i != "1" ]; do
                    if [[ ${RESP,,} == "s" ]]; then
                        chmod $CAMBIO${TIPO,,} $ARCHIVO 2> log.txt
                        #en caso de error sale inesperadamente.
                        if [ $? == "1" ]; then
                            echo "ERROR - ver log.txt" 
                            exit 1
                        fi
                        echo "El permiso de $ARCHIVO ha sido cambiado."
                        i=1
                    elif [[ ${RESP,,} == "n" ]]; then
                        #se canceló el cambio de propietario
                        echo "Ningun permiso ha sido modificado."
                        i=1
                    else
                        #no avanza hasta que responda correctamente.
                        read -p "Ingrese 's' para cambiar el permiso o 'n' para cancelar: " RESP
                    fi
                done
            else
                echo "Se asignará el permiso ${PERMISO,,}$CAMBIO${TIPO,,} al archivo o directorio $ARCHIVO.
                [U=Usuario,G=Grupo,O=Otros,A=Todos]
                ['+'=Añadir,'-'=Quitar]
                [r=Lectura,w=Escritura,x=Ejecución]"
                read -p "¿Desea continuar?[s=Si/n=No]" RESP
                i=0
                while [ $i != "1" ]; do
                    if [[ ${RESP,,} == "s" ]]; then
                        chmod ${PERMISO,,}$CAMBIO${TIPO,,} $ARCHIVO 2> log.txt
                        #en caso de error sale inesperadamente.
                        if [ $? == "1" ]; then
                            echo "ERROR - ver log.txt" 
                            exit 1
                        fi
                        echo "El permiso de $ARCHIVO ha sido cambiado."
                        i=1
                    elif [[ ${RESP,,} == "n" ]]; then
                        #se canceló el cambio de propietario
                        echo "Ningun permiso ha sido modificado."
                        i=1
                    else
                        #no avanza hasta que responda correctamente.
                        read -p "Ingrese 's' para cambiar el permiso o 'n' para cancelar: " RESP
                    fi
                done
            fi

            #Pregunta al usuario si quiere salir o volver al menu
            read -p "¿Quiere Salir o volver al menu principal?[s=Salir/m=Menu] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "s" ]]; then
                    exit 0
                elif [[ ${RESP,,} == "m" ]]; then
                    #vuelve al menu principal
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    read -p "Ingrese 's' para Salir o 'm' para volver al Menu: " RESP
                fi
            done
        else
            #si el archivo no existe pregunta por salir o volver al menu.
            echo "El archivo o directorio no existe."
            read -p "¿Quiere Salir o volver al menu principal?[s=Salir/m=Menu] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "s" ]]; then
                    exit 0
                elif [[ ${RESP,,} == "m" ]]; then
                    #vuelve al menu principal
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    read -p "Ingrese 's' para Salir o 'm' para continuar: " RESP
                fi
            done
        fi

    elif [[ $RESP == "2" ]]; then
        #opcion 2: Cambiar dueño de archivo o directorio elegido por el usuario.
        echo -e "\nCambiar propietario de archivos o directorios."
        if [ $UID -eq 0 ]; then
            read -ep "Ingrese el nombre del archivo: " ARCHIVO

            #verifica existencia del archivo
            if [ -e "$ARCHIVO" ]; then
                #el archivo existe
                read -p "¿Que usuario sera el propietario de $ARCHIVO? " USUARIO

                #verifica si el usuario existe
                if ( cut -d ":" -f 1 /etc/passwd | grep -wq "$USUARIO" ) ;then
                    #El usuario existe
                    #Pregunta el grupo
                    read -p "¿Que grupo de usuarios sera el propietario de $ARCHIVO? " GRUPO
                        
                    #Verifica si el grupo existe
                    if ( cut -d ":" -f 1 /etc/group | grep -wq "$GRUPO" ) ;then
                        #El grupo existe
                        #Verifica si se quiere hacer el cambio
                        read -p "Se asignara el propietario de $ARCHIVO al usuario $USUARIO y al grupo $GRUPO.¿Desea continuar?[s=Si/n=No] " RESP
                        i=0
                        while [ $i != "1" ]; do
                            if [[ ${RESP,,} == "s" ]]; then
                                #realiza el cambio de dueño del archivo elegido
                                chown -R $USUARIO:$GRUPO $ARCHIVO 2> log.txt
                                #en caso de error sale inesperadamente.
                                if [ $? == "1" ]; then
                                    echo "ERROR - ver log.txt"
                                    exit 1
                                fi
                                echo "El permiso de $ARCHIVO ha sido cambiado."
                                i=1
                            elif [[ ${RESP,,} == "n" ]]; then
                                #se canceló el cambio de propietario
                                echo "No se realizo el cambio del permiso."
                                i=1
                            else
                                #no avanza hasta que responda correctamente.
                                read -p "Ingrese 's' para cambiar el permiso o 'n' para cancelar: " RESP
                            fi
                        done
                    else
                        echo "El grupo $GRUPO no existe. Puede crearlo utilizando la opcion 3 del menu principal y listar los existentes con la opcion 4."
                    fi
                else
                    echo "El usuario $USUARIO no existe."
                fi
            else
                #si el archivo no existe pregunta por salir o volver al menu.
                echo "El archivo o directorio $ARCHIVO no existe."
            fi
        else
            echo -e "En esta seccion solo se puede utilizar si $0 es ejecutado como root."
        fi

        #pregunta para salir o volver al menu
        read -p "¿Quiere Salir o volver al menu principal?[s=Salir/m=Menu] " RESP
        i=0
        while [ $i != "1" ]; do
            if [[ ${RESP,,} == "s" ]]; then
                exit 0
            elif [[ ${RESP,,} == "m" ]]; then
                #vuelve al menu principal
                i=1
            else
                #no avanza hasta que responda correctamente.
                read -p "Ingrese 's' para Salir o 'm' para continuar: " RESP
            fi
        done
        
    elif [[ $RESP == "3" ]]; then
        #opcion 3: Permite crear o eliminar grupos.
        echo -e "\nCrear o eliminar grupos."
        if [ $UID -eq 0 ]; then
            read -p "¿Que accion quiere realizar?[c=Crear/e=Eliminar] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "c" ]]; then
                    #Se creara un grupo.
                    read -p "Ingrese el nombre del grupo a crear: " GRUPO
                    #Verifica si el grupo existe
                    if ( cut -d ":" -f 1 /etc/group | grep -wq "$GRUPO" ) ;then
                        #El grupo existe
                        echo "El grupo ya existe. Puede utilizar la opcion 4 del Menu principal para listar los grupos existentes."
                        i=1
                    else
                        read -p "Se creara el grupo $GRUPO. ¿Quiere continuar?[s=Si/n=No] " RESP
                        while [ $i != "1" ]; do
                            if [[ ${RESP,,} == "s" ]]; then
                                #Se crea el grupo
                                groupadd $GRUPO 2> log.txt
                                #en caso de error sale inesperadamente.
                                if [ $? == "1" ]; then
                                    echo "ERROR - ver log.txt" 
                                    exit 1
                                fi
                                echo "Se creó el grupo $GRUPO."
                                i=1
                            elif [[ ${RESP,,} == "n" ]]; then
                                #se cancela la creacion del grupo
                                echo "No se creó el grupo $GRUPO."
                                i=1
                            else
                                #no avanza hasta que responda correctamente.
                                read -p "Ingrese 's' para continuar o 'n' para cancelar: " RESP
                            fi
                        done
                    fi
                elif [[ ${RESP,,} == "e" ]]; then
                    #Se eliminara un grupo.
                    read -p "Ingrese el nombre del grupo a eliminar: " GRUPO
                    #Verifica si el grupo existe
                    if ( cut -d ":" -f 1 /etc/group | grep -wq "$GRUPO" ) ;then
                        #El grupo existe
                        read -p "Se eliminara el grupo $GRUPO. ¿Quiere continuar?[s=Si/n=No] " RESP
                        while [ $i != "1" ]; do
                            if [[ ${RESP,,} == "s" ]]; then
                                groupdel $GRUPO
                                echo "Se eliminó el grupo $GRUPO."
                                i=1
                            elif [[ ${RESP,,} == "n" ]]; then
                                #vuelve al menu principal
                                echo "No se eliminó el grupo $GRUPO."
                                i=1
                            else
                                #no avanza hasta que responda correctamente.
                                read -p "Ingrese 's' para continuar o 'n' para cancelar: " RESP
                            fi
                        done
                    else
                        echo "El grupo no existe. Puede utilizar la opcion 4 del Menu principal para listar los grupos existentes."
                        i=1
                    fi
                else
                    #no avanza hasta que responda correctamente.
                    echo "Ingrese una opcion correcta."
                fi
            done
        else
            echo -e "En esta seccion solo se puede utilizar si $0 es ejecutado como root."
        fi

        #Pregunta al usuario si quiere salir o volver al menu
        read -p "¿Quiere Salir o volver al menu principal?[s=Salir/m=Menu] " RESP
        i=0
        while [ $i != "1" ]; do
            if [[ ${RESP,,} == "s" ]]; then
                exit 0
            elif [[ ${RESP,,} == "m" ]]; then
                 #vuelve al menu principal
                i=1
            else
                #no avanza hasta que responda correctamente.
                read -p "Ingrese 's' para Salir o 'm' para volver al Menu: " RESP
            fi
        done

    elif [[ $RESP == "4" ]]; then
        #opcion 4: lista los grupos de usuario existentes
        echo -e "\nLos grupos existentes son: "
        cut -d ":" -f 1 /etc/group

        #Pregunta al usuario si quiere salir o volver al menu
        read -p "¿Quiere Salir o volver al menu principal?[s=Salir/m=Menu] " RESP
        i=0
        while [ $i != "1" ]; do
            if [[ ${RESP,,} == "s" ]]; then
                exit 0
            elif [[ ${RESP,,} == "m" ]]; then
                #vuelve al menu principal
                i=1
            else
                #no avanza hasta que responda correctamente.
                read -p "Ingrese 's' para Salir o 'm' para volver al Menu: " RESP
            fi
        done
            
    elif [[ $RESP == "5" ]]; then
        #opcion 5: busca fichero y directorios por permisos definido por el usuario.
        echo -e "\nBuscar Fichero o directorio."

        #si no es root el que ejecuta muestra un mensaje recordando que tendra limitaciones.
        if [ ! $UID -eq 0 ]; then
            echo "Recuerde que si no ejecuta el srcipt como root no va a poder buscar en todos los directorios."
        fi
        #preguntar el tipo de permiso de usuario propietario
        echo "Defina los Parametros de busqueda" /n "Elija el permiso para Usuario."
        USU=0
        i=0
        while [ $i != "1" ]; do
            #preguntar el tipo de permiso
            echo "¿Que tipo de permiso quiere buscar?"
            read -p "¿Buscar permiso de Lectura?[s=Si/N=No] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "s" ]]; then
                    i=1
                    USU=$(($USU + 4))
                elif [[ ${RESP,,} == "n" ]]; then
                    #avanza sin buscar permiso de lectura
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    read -p "Ingrese una opcion correcta. " RESP
                fi
            done

            read -p "¿Buscar permiso de Escritura?[s=Si/N=No] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "s" ]]; then
                    i=1
                    USU=$(($USU + 2))
                elif [[ ${RESP,,} == "n" ]]; then
                    #avanza sin buscar permiso de escritura
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    read -p "Ingrese una opcion correcta. " RESP
                fi
            done

            read -p "¿Buscar permiso de Ejecucion?[s=Si/N=No] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "s" ]]; then
                    i=1
                    USU=$(($USU + 1))
                elif [[ ${RESP,,} == "n" ]]; then
                    #avanza sin buscar permiso de escritura
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    read -p "Ingrese una opcion correcta. " RESP
                fi
            done
        done

        #preguntar el tipo de permiso de grupo
        echo "Elija el permiso para Grupo."
        GRU=0
        i=0
        while [ $i != "1" ]; do
            #preguntar el tipo de permiso
            echo "¿Que tipo de permiso quiere buscar?"
            read -p "¿Buscar permiso de Lectura?[s=Si/N=No] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "s" ]]; then
                    i=1
                    GRU=$(($GRU + 4))
                elif [[ ${RESP,,} == "n" ]]; then
                    #avanza sin buscar permiso de lectura
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    read -p "Ingrese una opcion correcta. " RESP
                fi
            done

            read -p "¿Buscar permiso de Escritura?[s=Si/N=No] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "s" ]]; then
                    i=1
                    GRU=$(($GRU + 2))
                elif [[ ${RESP,,} == "n" ]]; then
                    #avanza sin buscar permiso de escritura
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    read -p "Ingrese una opcion correcta. " RESP
                fi
            done

            read -p "¿Buscar permiso de Ejecucion?[s=Si/N=No] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "s" ]]; then
                    i=1
                    GRU=$(($GRU + 1))
                elif [[ ${RESP,,} == "n" ]]; then
                    #avanza sin buscar permiso de escritura
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    read -p "Ingrese una opcion correcta. " RESP
                fi
            done
        done

        #preguntar el tipo de permiso de Otros
        echo "Elija el permiso para Otros."
        OTR=0
        i=0
        while [ $i != "1" ]; do
            #preguntar el tipo de permiso
            echo "¿Que tipo de permiso quiere buscar?"
            read -p "¿Buscar permiso de Lectura?[s=Si/N=No] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "s" ]]; then
                    i=1
                    OTR=$(($OTR + 4))
                elif [[ ${RESP,,} == "n" ]]; then
                    #avanza sin buscar permiso de lectura
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    read -p "Ingrese una opcion correcta. " RESP
                fi
            done

            read -p "¿Buscar permiso de Escritura?[s=Si/N=No] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "s" ]]; then
                    i=1
                    OTR=$(($OTR + 2))
                elif [[ ${RESP,,} == "n" ]]; then
                    #avanza sin buscar permiso de escritura
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    read -p "Ingrese una opcion correcta. " RESP
                fi
            done

            read -p "¿Buscar permiso de Ejecucion?[s=Si/N=No] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "s" ]]; then
                    i=1
                    OTR=$(($OTR + 1))
                elif [[ ${RESP,,} == "n" ]]; then
                    #avanza sin buscar permiso de escritura
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    read -p "Ingrese una opcion correcta. " RESP
                fi
            done
        done

        #donde buscar
        i=0
        while [ $i != "1" ]; do
            read -ep "¿Desde que directorio quiere comenzar la busqueda? " DIR
            #verifica existencia del archivo
            if [ -e $DIR ]; then
                #si el directorio existe, sigue avanzando.
                i=1
            else
                echo "Directorio inexistente."
                #Pregunta al usuario si quiere salir o volver al menu
                read -p "¿Quiere Salir o volver al menu principal?[s=Salir/m=Menu] " RESP
                while [ $i != "1" ]; do
                    if [[ ${RESP,,} == "s" ]]; then
                        exit 0
                    elif [[ ${RESP,,} == "m" ]]; then
                        #vuelve al menu principal
                        i=1
                    else
                        #no avanza hasta que responda correctamente.
                        read -p "Ingrese 's' para Salir o 'm' para volver al Menu: " RESP
                    fi
                done
            fi
        done

        if [[ ${RESP,,} != "m" ]]; then
            echo "Los archivos encontrados son: "
            #realiza la busqueda permiso elegido
            find $DIR -perm $USU$GRU$OTR 2> log.txt
            #en caso de error sale inesperadamente.
            if [ $? == "1" ]; then
                echo "ERROR - ver log.txt" 
                exit 1
            fi
            #Pregunta al usuario si quiere salir o volver al menu
            read -p "¿Quiere Salir o volver al menu principal?[s=Salir/m=Menu] " RESP
            i=0
            while [ $i != "1" ]; do
                if [[ ${RESP,,} == "s" ]]; then
                    exit 0
                elif [[ ${RESP,,} == "m" ]]; then
                    #vuelve al menu principal
                    i=1
                else
                    #no avanza hasta que responda correctamente.
                    read -p "Ingrese 's' para Salir o 'm' para volver al Menu: " RESP
                fi
            done
        fi
        
    elif [[ $RESP == "6" ]]; then
        #opcion 5: Sale
        exit 0
    else
        read -p "Ingrese una opción válida."
    fi
done
